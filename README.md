At Shift Chiropractic, we focus on chiropractic and overall health well being for the entire family. We provide neurologically based upper cervical chiropractic care utilizing state-of-the-art technology to pinpoint the root cause of a variety of health conditions.

Address: 3055 Cass Rd, Ste 102B, Traverse City, MI 49684, USA

Phone: 231-846-8897
